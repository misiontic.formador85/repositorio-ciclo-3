# Ejercicio de diseño de bases de datos - Enterprise Bank

Te contratan para diseñar la base de datos de un pequeña entidad bancaria. El banco tiene sucursales, donde, cada sucursal esta caracterizada por tener el nombre, ciudad y el número de contacto. El banco también ya tiene la información previa de todos sus clientes, teniendo en cuenta que los datos de los clientes son: Nombre, dirección de residencia, ciudad y codigo postal. Cada cliente puede tener cuentas en el banco, por lo que cada cuenta tiene el número, el saldo, nombre del titular y el tipo de cuenta (ahorros o corriente).

</br>

Ahora, el banco indica que quiere guardar los datos de los empleados, así que para cada empleado se tiene el tipo y número de documento, nombres, dirección, números de contacto, salario, el tiempo en el que empieza a trabajar y el tiempo estimado de trabajo.

</br>

Se debe tener en cuenta en la base de datos los registros de los prestamos (indicando la monto prestado) y pagos (indicando la fecha de pago y el monto pagado).

## Actividades a desarrollar

### Diseño de base de datos en papel

1. Haz una lista de todas las entidades del problema.
2. Identifica posibles relaciones entre las entidades.
3. Relaciona cada una de las entidades del problema usando el modelo entidad-relación usando [notación de Chen](https://ewebik.com/base-de-datos/modelo-entidad-relacion).
4. Agrega la cardinalidad en todas las relaciones.
5. Agrega a las entidades los diferentes atributos y distingue la llave primaria.

### Diseño en MySQL Workbench

1. Usa el diseño anterior para crear el modelo de la base de datos en *MySQL Workbench*.

### Implementación

1. Usa Forward Engineering para migrar del modelo al servidor de *MySQL*