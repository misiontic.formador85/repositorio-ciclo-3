# Ejercicio Colegio

![modelo](modelo_v2.png)

Eres parte de un proyecto para una institución educativa. Te entregan el modelo entidad relación y te piden crear un microservicio.

## Actividades a desarrollar

### Entidades y Repositorios

1. Crear proyecto de Spring
2. Crear paquete entities
3. Crear entidades a partir del diagrama
4. Crear un repositorio para cada entidad
5. Usar pruebas unitarias para crear y buscar entidades.

### Controlador

1. Crear un controlador y realizar inyección de dependencias de los repositorios a usar.
2. Realizar los endpoints según las siguientes historias de usuario:
   
- Como Administrador quiero crear cursos para poder ofrecer cursos en el sistema.
- Como Administrador quiero crear usuarios para poder asignarles profesores.
- Como Administrador quiero crear profesores para poder registrar y asignar profesores en el sistema.
- Como Profesor quiero ver el listado 

## Actividades extra

Hacer que los repositorios puedan consultar:​

- Un estudiante por nombres y apellidos​

- Imprimir la lista de profesores del estudiante consultado​

- Consultar los cursos según el código tal que contengan la letra O