function getProductos(){
    let peticion = new XMLHttpRequest()
    peticion.open("GET", "https://minticloud.uis.edu.co/c3s51formador/get-productos")
    peticion.responseType = "json"
    peticion.send()

    peticion.onload = function(){
        console.log(peticion.response)

        const array = peticion.response

        let filasTabla = `<th>ID</th>
        <th>Nombre</th>
        <th>Temperatura</th>
        <th>Valor Base</th>
        <th>Acciones</th>`

        for (let i = 0; i < array.length; i++) {
            filasTabla += `<tr>
                <td>${ array[i].id }</td>
                <td>${ array[i].nombre }</td>
                <td>${ array[i].temperatura }</td>
                <td>${ array[i].precio }</td>
                <td><a href="/modificar-producto.html?id=${ array[i].id }">Modificar</a></td>
            </tr>`
            
        }
        document.getElementById("table").innerHTML = filasTabla

    }

}

getProductos()