function borrarProducto() {
    const producto = {
        id: document.querySelector("#idInput").value, 
    }

    console.log(producto)


    // CREAR LA PETICION
    let peticion = new XMLHttpRequest()
    peticion.open("POST", "https://minticloud.uis.edu.co/c3s51formador/borrar-producto")
    peticion.setRequestHeader("Content-Type","application/json")
    peticion.responseType = 'json'
    peticion.send(JSON.stringify(producto))

    peticion.onload = function(){
        window.location.replace("https://minticloud.uis.edu.co/c3s51formador/listar-productos.html")
    }

}

function modificarProducto() {

    if (document.getElementById("nombreInput").value === ""){
        document.getElementById("nombreInput").classList.add("is-invalid")
    } else {

        const producto = {
            id: document.getElementById("idInput").value,
            nombre: document.getElementById("nombreInput").value,
            temperatura: document.getElementById("temperaturaInput").value,
            precio: document.getElementById("valorBaseInput").value,
        }
    
        console.log(producto)
    
    
        // CREAR LA PETICION
        let peticion = new XMLHttpRequest()
        peticion.open("POST", "https://minticloud.uis.edu.co/c3s51formador/save-producto")
        peticion.setRequestHeader("Content-Type","application/json")
        peticion.responseType = 'json'
        peticion.send(JSON.stringify(producto))
    
        peticion.onload = function(){
    
            console.log(peticion.response)
            window.location.replace("https://minticloud.uis.edu.co/c3s51formador/listar-productos.html")
    
        }

    }




}

const queryString = window.location.search
const urlParams = new URLSearchParams(queryString)

if (urlParams.has("id")) { // En este caso id es en nombre del parametro
    const id = urlParams.get("id") // ahora la constante id tiene el parámetro

    let peticion = new XMLHttpRequest()
    peticion.open("GET", "https://minticloud.uis.edu.co/c3s51formador/get-producto?id=" + id)
    peticion.responseType = "json"
    peticion.send()

    peticion.onload = function(){

        const producto = peticion.response

        document.getElementById("idInput").value = producto.id
        document.getElementById("nombreInput").value = producto.nombre
        document.getElementById("temperaturaInput").value = producto.temperatura
        document.getElementById("valorBaseInput").value = producto.precio


    }

}
