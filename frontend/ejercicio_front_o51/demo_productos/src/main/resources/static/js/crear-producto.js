function crearProducto() {

    if (document.getElementById("nombreInput").value === ""){
        document.getElementById("nombreInput").classList.add("is-invalid")
    } else {

        const producto = {
            nombre: document.getElementById("nombreInput").value,
            temperatura: document.getElementById("temperaturaInput").value,
            precio: document.getElementById("valorBaseInput").value,
        }
    
        console.log(producto)
    
    
        // CREAR LA PETICION
        let peticion = new XMLHttpRequest()
        peticion.open("POST", "https://minticloud.uis.edu.co/c3s51formador/save-producto")
        peticion.setRequestHeader("Content-Type","application/json")
        peticion.responseType = 'json'
        peticion.send(JSON.stringify(producto))
    
        peticion.onload = function(){
    
            console.log(peticion.response)
    
        }

    }

}