package com.misiontic.demo.dao;

import org.springframework.data.repository.CrudRepository;

import com.misiontic.demo.entities.Producto;

public interface ProductoRepo extends CrudRepository<Producto, Integer> {
    
    Iterable<Producto> findByPrecioGreaterThan(int precio);

}
