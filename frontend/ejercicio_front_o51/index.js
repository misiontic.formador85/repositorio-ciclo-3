let nombre = "MiFarmacia"

console.log("Bienvenido a " + nombre)

function crearProducto() {
    const producto = {
        nombre: document.getElementById("nombreInput").value,
        id: document.querySelector("#idInput").value, 
        temperatura: document.getElementById("temperaturaInput").value,
        valorBase: document.getElementById("valorBaseInput").value,
    }

    console.log(producto)

    let peticion = new XMLHttpRequest()
    peticion.open("POST", "http://localhost:8080/crear-producto")
    peticion.setRequestHeader("Content-Type","application/json")
    peticion.responseType = 'json'
    peticion.send(JSON.stringify(producto))
    peticion.onload = function(){

        console.log(peticion.response)

    }

}

crearProducto()

