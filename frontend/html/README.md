# Lenguaje HTML

La forma en que vemos las interfaces web es por medio de un lenguaje HTML. En [W3School](https://www.w3schools.com/html/default.asp) encontraremos una de las mejores guias para implementar HTML.

# Formularios

Los formularios de HTML manejan atributos para validar el campo a ingresar. Deben estructurarse con la etiqueta `<form></form>` donde se indica el método a usar y la URL en la cual se redirige la petición al hacer click en el botón con el tipo `submit`.

