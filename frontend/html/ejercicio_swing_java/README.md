# De SWING a HTML

![Reto 5](image.png)

¿Recuerdas el reto 5 del ciclo 2? A continuación vamos a migrar del proyecto anterior (Mostrado en la imagen) para adaptarlo en un sitio web.
 
## Elementos HTML a tener en cuenta

- [Tablas](https://www.w3schools.com/html/html_table_borders.asp)
- [Formularios](https://www.w3schools.com/html/html_forms.asp)

## Actividades a desarrollar

1. Crear el formulario sin usar estilos
2. Crear la tabla con unos datos predefinidos
