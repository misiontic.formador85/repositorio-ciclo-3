# Gestión de usuarios

Deseas agregar gestión de usuarios y autenticación a tu aplicación Web, por esta razón, usas en el backend una entidad usuario (que tiene: nombre, apellido, email, celular, clave y fechaCreacion) para exponer diferentes EndPoints con el fin de hacer un CRUD completo.

## Actividades a desarrollar

1. Crea un microservicio que permita hacer un CRUD de la entidad usuario.
2. Crea una pantalla para realizar el registro de un usuario
3. Crea una pantalla para listar los usuarios, donde cada usuario tenga un link para redirigir a otra pantalla de modificación
4. Crea la pantalla de modificacion de usuario.

## Login

1. Crea una [plantilla según Bootstrap](https://getbootstrap.com/docs/5.2/examples/sign-in/) para validar el login de un usuario 
2. Usa peticiones para enviar el formulario de login a un endpoint.
3. En el endpoint usa el objeto recibido para validar el email y clave contra la base de datos, en respuesta procura crear una instancia que envie un resultado como `{"auth":"true"}`
4. Usa la respuesta para redirigir a la pagina home de la aplicación. Usa también la respuesta para restringir el acceso si no es exitoso.