# Lenguaje CSS

La forma en que vemos las interfaces web es por medio de un lenguaje HTML. Sin embargo, el HTML siempre va acompañado de estilos, los estilos se implementan en CSS por medio de selectores y propiedades. En [W3School](https://www.w3schools.com/css/default.asp) encontraremos una de las mejores guias para implementar HTML + CSS.

## Como organizar elementos usando Flex

Por medio del layout Flex podemos mostrar los elementos según las propiedades del contenedor. A continuación encontramos la [guía para implementar Flex](https://css-tricks.com/snippets/css/a-guide-to-flexbox/).

## Utilizando estilos predefinidos con Bootstrap

[Bootstrap es un framework](https://getbootstrap.com/) de CSS, es decir, tiene clases de estilos listas para implementar en un proyecto Frontend. Separa los elementos en componentes de HTML.

### [Layouts en Bootstrap](https://getbootstrap.com/docs/5.2/layout/breakpoints/)

Antes de ver layouts de Bootstrap revisar el layout Flex. Bootstrap proporciona clases predefinidas para organizar los diferentes elementos por medio de filas y columnas. Los layouts (es decir, la forma en que se organizan los elementos HTML) usan breakpoints, esto es, que cambia el layout según el tamaño de la pantalla. Recordar que, los layout de Bootstrap se pueden dividir en máximo 12 columnas.

### [Formularios en Bootstrap](https://www.w3schools.com/bootstrap5/bootstrap_forms.php)

Los formularios tienen un estilo predefinido de bootstrap. Al tener integrado un modulo de JS de Bootstrap, es posible usar una sola clase "was-validated" que cambia de color los campos cuando son validos o invalidos.

### [Botones en Bootstrap](https://getbootstrap.com/docs/5.2/components/buttons/)

Bootstrap proporciona una serie de clases para agregar estilo a los botones basandose en colores predefinidos.

### [Margenes y Padding](https://getbootstrap.com/docs/5.2/utilities/spacing/#margin-and-padding)

De la misma forma, por medio de clases, bootstrap agrega margenes o espaciados internos (padding) a los componentes. Además, se puede agregar a la izquierda, derecha, arriba o abajo. Cada margen se aplica usando un número, indicando que tanto espacio queremos agregar (los números van en un rango de 1 a 5) ejemplo: `mt-1` margen arriba.

### [Tablas](https://www.w3schools.com/bootstrap5/bootstrap_tables.php)

Las tablas tienen estilos, se aplican como se muestra en la guía.

### [Barras de navegación](https://www.w3schools.com/bootstrap5/bootstrap_navbar.php)

Las barras de navegación son un componente que la mayoria de páginas web tienen en común.