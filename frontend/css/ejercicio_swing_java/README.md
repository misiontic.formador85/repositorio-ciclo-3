# De SWING a HTML + CSS

![Reto 5](image.png)

¿Recuerdas el reto 5 del ciclo 2? A continuación vamos a migrar del proyecto anterior (Mostrado en la imagen) para adaptarlo en un sitio web.
 
## Elementos HTML a tener en cuenta

- [Introducción a los estilos](https://www.w3schools.com/html/html_css.asp)
- [Selectores](https://www.w3schools.com/css/css_selectors.asp)
- [Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
- [Framework de CSS - Bootstrap](https://getbootstrap.com/docs/5.2/getting-started/introduction/)

## Actividades a desarrollar

1. Añadir estilos a la tabla.
2. Usar la propiedad display: flex para acomodar los componentes del formulario y la tabla según la imagen.
3. Añadir estilos bootstrap.
4. Reemplazar el layout basado en flex puro a grid de bootstrap, [ver más en Bootstrap](https://getbootstrap.com/docs/4.0/layout/grid/).