package com.misiontic.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.misiontic.demo.dao.ProductoRepo;
import com.misiontic.demo.entities.Producto;

@CrossOrigin(allowedHeaders = "*", origins = "*")
@RestController
public class ProductoController {
    
    @Autowired
    private ProductoRepo productoRepo;

    @PostMapping("/save-producto")
    public Producto saveProducto(@RequestBody Producto producto){
        return productoRepo.save(producto);
    }


    @PostMapping("/borrar-producto")
    public void borrarProducto(@RequestBody Producto producto){
        productoRepo.deleteById(producto.getId());
    }

    @GetMapping("/get-producto")
    public Producto getProducto(@RequestParam int id){
        return productoRepo.findById(id).get();
    }
   
    @GetMapping("/get-productos")
    public Iterable<Producto> getProductos(){
        return productoRepo.findAll();
    }

}
