function crearProducto() {

    const producto = {
        nombre: document.getElementById("nombreInput").value,
        temperatura: document.querySelector("#temperaturaInput").value,
        precio: document.getElementById("valorBaseInput").value
    }

    let peticion = new XMLHttpRequest()
    peticion.open("POST", "http://localhost:8080/save-producto")
    peticion.setRequestHeader("Content-Type", "application/json")
    peticion.responseType = 'json'

    peticion.send(JSON.stringify(producto))

    peticion.onload = function () {

        console.log(peticion.response)
        window.location.replace("http://localhost:8080/listar-productos.html")
        
    }

}