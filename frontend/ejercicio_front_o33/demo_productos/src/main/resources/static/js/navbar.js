const navbar = `<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
<div class="container-fluid">
    <a class="navbar-brand" href="/">MisionTIC</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor02"
        aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarColor02">
        <ul class="navbar-nav me-auto">
            <li class="nav-item">
                <a class="nav-link active" href="/">Inicio
                    <span class="visually-hidden">(current)</span>
                </a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="/" role="button"
                    aria-haspopup="true" aria-expanded="false">Productos</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="/listar-productos.html">Lista de productos</a>
                    <a class="dropdown-item" href="/crear-producto.html">Crear producto</a>
                </div>
            </li>
        </ul>
        <form class="d-flex">
            <button class="btn btn-secondary my-2 my-sm-0" type="submit">Cerrar sesión</button>
        </form>
    </div>
</div>
</nav>`

document.getElementById("navbar").innerHTML = navbar