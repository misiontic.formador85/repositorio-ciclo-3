function borrarProducto() {

    const producto = {
        id: document.getElementById("idInput").value,
    }
    console.log("PRODUCTO")
    console.log(producto)

    let peticion = new XMLHttpRequest()
    peticion.open("POST", "http://localhost:8080/borrar-producto")
    peticion.setRequestHeader("Content-Type", "application/json")
    peticion.responseType = 'json'

    peticion.send(JSON.stringify(producto))

    peticion.onload = function () {

        console.log(peticion.response)
        window.location.replace("http://localhost:8080/listar-productos.html")

    }

}


function modificarProducto() {

    const producto = {
        id: document.getElementById("idInput").value,
        nombre: document.getElementById("nombreInput").value,
        temperatura: document.querySelector("#temperaturaInput").value,
        precio: document.getElementById("valorBaseInput").value
    }

    let peticion = new XMLHttpRequest()
    peticion.open("POST", "http://localhost:8080/save-producto")
    peticion.setRequestHeader("Content-Type", "application/json")
    peticion.responseType = 'json'

    peticion.send(JSON.stringify(producto))

    peticion.onload = function () {

        console.log(peticion.response)
        window.location.replace("http://localhost:8080/listar-productos.html")

    }

}

const queryString = window.location.search
const urlParams = new URLSearchParams(queryString)

if (urlParams.has("id")) { // En este caso id es en nombre del parametro
    const id = urlParams.get("id") // ahora la constante id tiene el parámetro
    document.getElementById("idInput").value = id

    const peticion = new XMLHttpRequest()
    peticion.open("GET","http://localhost:8080/get-producto?id=" + id )
    peticion.responseType = "json"
    peticion.send()

    peticion.onload = function(){

        const respuesta = peticion.response

        document.getElementById("nombreInput").value = respuesta.nombre
        document.querySelector("#temperaturaInput").value = respuesta.temperatura
        document.getElementById("valorBaseInput").value = respuesta.precio

    }

}





