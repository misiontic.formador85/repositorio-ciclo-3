function getProductos() {

    let req = new XMLHttpRequest()

    req.open("GET", "http://127.0.0.1:8080/get-productos")
    req.responseType = "json"

    req.send()

    req.onload = function () {

        console.log(req.response)

        const respuesta = req.response

        for (let i = 0; i < respuesta.length; i++) {
            document.getElementById("table").innerHTML += `
                <tr>
                    <td>${respuesta[i].id}</td>
                    <td>${respuesta[i].nombre}</td>
                    <td>${respuesta[i].temperatura}</td>
                    <td>${respuesta[i].precio}</td>
                    <td><a class="link-primary" href="/modificar-producto.html?id=${respuesta[i].id}">Modificar</a></td>
                </tr>`
        }

    }

}

getProductos()
