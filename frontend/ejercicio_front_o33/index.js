let nombre = "MI FARMACIA"

console.log("Hola, " + nombre)

nombre = "FARMACIA 2.0"

function crearProducto() {

    const producto = { id: document.getElementById("idInput").value,
        nombre:  document.getElementById("nombreInput").value,
        temperatura: document.querySelector("#temperaturaInput").value,
        valorBase: document.getElementById("valorBaseInput").value }

    console.log( producto )

    let peticion = new XMLHttpRequest()
    peticion.open("POST", "http://localhost:8080/crear-producto")
    peticion.setRequestHeader("Content-Type","application/json")
    peticion.responseType = 'json'

    peticion.send( JSON.stringify(producto) )

    peticion.onload = function() {
        
        console.log(peticion.response)
        
    }

}
