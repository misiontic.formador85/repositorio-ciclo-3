# De SWING a HTML + CSS + JS

![Reto 5](image.png)

¿Recuerdas el reto 5 del ciclo 2? A continuación vamos a migrar del proyecto anterior (Mostrado en la imagen) para adaptarlo en un sitio web.
 
## Elementos HTML a tener en cuenta

- [Declaración de variables](https://www.w3schools.com/js/js_let.asp)
- [Funciones](https://www.w3schools.com/js/js_functions.asp)
- [Eventos](https://www.w3schools.com/js/js_events.asp)
- [Objetos](https://www.w3schools.com/js/js_events.asp)

## Actividades a desarrollar

1. Recoger los campos del formulario al hacer click
2. Guardar los campos en un objeto de JS
3. Usar consola para verificar los campos recogidos
4. Usar AJAX para enviar los datos al backend. (Ojo, crear un backend que solo tenga un controlador para recibir los datos)
   
```
let xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
xmlhttp.open("POST", "/json-handler");
xmlhttp.setRequestHeader("Content-Type", "application/json");
xmlhttp.send(JSON.stringify({name:"John Rambo", time:"2pm"}));
```

5. Verificar el request y response en consola.