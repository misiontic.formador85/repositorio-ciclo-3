# Lenguaje JAVASCRIPT

El desarrollo Frontend necesita un lenguaje para escuchar eventos, realizar peticiones al backend, implementar seguridad, etc. Todo esto se desarrolla usando Javascript. Es un lenguaje de tipado débil que permite interactuar con HTML por medio del [DOM (Document Object Model)](https://www.w3schools.com/js/js_htmldom.asp).

## Crear variables de JS

Para crear variables se usa *keywords* `var, const, let` seguido del nombre de la variable y se asigna el valor con `=`. Ejemplo, `let conteo = 5`. Los punto y coma al terminar una sentencia son opcionales.

## [Objetos en JS](https://www.w3schools.com/js/js_objects.asp)

Javascript permite declarar variables como objetos. Es decir, se declaran directamente sin necesidad de crear una clase para instanciar. El formato JSON se basa en la forma en la que se escriben objetos en Javascript.

## [Funciones en JS](https://www.w3schools.com/js/js_function_definition.asp)

Las funciones son un elemento común en proyetos Frontend. Se suelen usar para encapsular código que luego se llamará según un evento dado en el HTML.

## [Eventos](https://www.w3schools.com/jsref/event_onclick.asp)

Los eventos surgen de la interacción del cliente con la página web. Se pueden escuchar diferentes tipos de eventos. Sin embargo, en este caso únicamente vamos a escuchar los eventos `onclick` para ejecutar una acción con JS.

## [JS y el DOM de HTML](https://www.w3schools.com/js/js_htmldom_elements.asp)

Javascript puede consultar por los elementos de un documento HTML. Se usan funciones para consultar, asignar y obtener elementos, sus atributos o propiedades.

## [Usando JS para realizar petición](https://www.w3schools.com/js/js_ajax_intro.asp)

Se usa AJAX para realizar peticiones asincronas hacia un servidor. Para realizar peticiones necesitamos las bases anteriores y seguir el paso a paso.

### Recopilar los datos del formulario HTML y guardar en un objeto JS

Creamos un método que obtenga los campos del formulario, para ello usamos un método de JS que se ejecuta al dar click en un botón.

En el HTML se tiene:
```
<form onclick="return false">
.
.
.
    <button onclick="enviarData()"></button>

</form>

```

En el JS se tiene:
```
function enviarData(){
    
    const data = {nombre: document.getElementById("nombreInput").value,
        temperatura: document.querySelector("#temperaturaInput").value}

    console.log(data)
}
```

### Usar un objeto AJAX para crear o modificar una entidad

Se usa una petición asincrona tal que se ejecuta cuando se obtiene los campos del formulario, por lo tanto en la funcion se tiene

   
```
function enviarData(){
    
    const data = {nombre: document.getElementById("nombreInput").value,
        temperatura: document.querySelector("#temperaturaInput").value}

    console.log(data)

    let solicitud = new XMLHttpRequest()
    solicitud.open("POST", "https://minticloud.uis.edu.co/c3s33formador/crear-producto")
    solicitud.setRequestHeader("Content-Type", "application/json")
    solicitud.responseType = 'json'
    
    solicitud.send(JSON.stringify(data))

    solicitud.onload = function() {
        const respuesta = solicitud.response
        console.log(respuesta)
    }

}

```

### Usar un objeto AJAX para obtener entidades

La petición es un ejemplo de obtención de datos del backend. En este caso solo se hace una impresión por consola

   
```
function getData(){
    
    let solicitud = new XMLHttpRequest()
    solicitud.open("GET", "https://minticloud.uis.edu.co/c3s33formador/get-producto")
    solicitud.responseType = 'json'
    
    solicitud.send()

    solicitud.onload = function() {
        const respuesta = solicitud.response
        console.log(respuesta)
    }

}

```

### Crear una tabla a partir de un arreglo de objetos de JS

A partir de la petición pasada para obtener entidades del backend, si la constante respuesta es un arreglo de objetos y la etiqueta tbody de la tabla HTML tiene el id "table-body", entonces para construir una tabla se tiene:

```
function getData(){
    
    let solicitud = new XMLHttpRequest()
    solicitud.open("GET", "https://minticloud.uis.edu.co/c3s33formador/get-producto")
    solicitud.responseType = 'json'
    
    solicitud.send()

    solicitud.onload = function() {
        const respuesta = solicitud.response
        

        let filasTabla = ""

        for (let i = 0; i < respuesta.length; i++) {
            filasTabla += `<tr>
                <td>${ respuesta[i].id }</td>
                <td>${ respuesta[i].nombre }</td>
                <td>${ respuesta[i].temperatura }</td>
                <td>${ respuesta[i].precio }</td>
            </tr>`
        }

        document.getElementById("table-body").innerHTML = filasTabla

    }

}

```

### Recuperar parametros de consulta por URL


```
const queryString = window.location.search
const urlParams = new URLSearchParams(queryString)

if (urlParams.has("id")) { // En este caso id es en nombre del parametro
  const id = urlParams.get("id") // ahora la constante id tiene el parámetro
}
```

### Agregar una clase desde JS

```
document.getElementById("link-home").classList.add('active')
```

### Moverse a otra página

```
window.location.replace("http://www.w3schools.com");
```