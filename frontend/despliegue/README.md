# Despliegue

## Adaptar el frontend

1. En el frontend cambia las URL `http://localhost:8080/` por la siguiente plantilla `https://minticloud.uis.edu.co/ACA_VA_EL_USUARIO_DE_LA_CREDENCIAL`. Busca las credenciales en la carpeta de documentos compartida (El link de la carpeta esta en el foro noticias del curso).

## Base de datos

1. Puedes dejar la opción `spring.jpa.hibernate.ddl-auto=create` para que spring cree la base de datos o revisa la sesion 28 para saber como se migra la base de datos usando MySQLWorkbench.

## Adaptar el backend

1. Según las credenciales de Minticloud modifica el archivo application.properties. Recuerda poner la bandera `?useSSL=false` al final de la url de la base de datos
2. Abre el archivo `pom.xml` que se encuentra en la raíz del proyecto backend. Ahora copia la siguente etiqueta `<packaging>war</packaging>` justo debajo de la etiqueta `<version>0.0.1-SNAPSHOT</version>`
3. Busca la etiqueta `<dependencies>`, dentro de ella copia la siguente dependencia:

```
<dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-tomcat</artifactId>
   <scope>provided</scope>
</dependency>
```

4. Cierra el `pom.xml`, recuerda los cambios que hiciste por si te quieres devolver. Ahora, busca la clase principal. La reconoces porque tiene el único método *main*. Haz que la clase principal herede de `extends SpringBootServletInitializer`. Quedará algo así (cambia según tu proyecto).
   
```
package com.misiontic.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class DemoApplication extends SpringBootServletInitializer  {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
```
5. Busca tu clase controlador y encima de la anotación `@RestController` coloca otra anotación: `@CrossOrigin(allowedHeaders = "*", origins = "*")`. Esto permitirá el acceso de peticiones desde el frontend.

6. Perfecto, ahora solo falta agregar una variable de entorno. Puedes buscar en internet como agregar variables de entorno. En este caso la variable que vas a agregar se llama: *JAVA_HOME* y para su valor busca el directorio donde esta instalado Java. (por ejemplo: C:\Program Files\Java\jdk-11.0.16)

7. En visual busca la pestaña MAVEN, abre la pestaña Lifecycle y ejecuta `clean` e `install`. El proyecto compilado se mostrará dentro de la carpeta target (tiene el nombre de SNAPSHOT)

## Carga de proyecto

1. Descarga el software Filezilla [haciendo clic aquí](https://filezilla-project.org/download.php?type=client) e instala en el computador.
2. Usa la ruta `sftp://minticloud.uis.edu.co` y las credenciales compartidas en la carpeta de documentos para conectarte.
3. De la sección anterior, busca la carpeta que tiene el nombre de SNAPSHOT (dentro del proyecto del backend compilado, carpeta target). Copia las carpetas *META-INF* y *WEB-INF* en la carpeta de minticloud.
4. Espera unos minutos hasta el servidor de minticloud se reinicie. Uala! El proyecto se debe contemplar en la ruta `https://minticloud.uis.edu.co/ACA_VA_EL_USUARIO_DE_LA_CREDENCIAL`. Ejemplo: `https://minticloud.uis.edu.co/c3s33formador`

