# Desarrollo Frontend

Tripulantes, encontrarán en cada carpeta ejercicios, guias o links de interés.

## Requerimientos de software

### Extensiones de Visual Studio Code

- [Live Preview](https://marketplace.visualstudio.com/items?itemName=ms-vscode.live-server)
- [IntelliSense for CSS class names in HTML](https://marketplace.visualstudio.com/items?itemName=Zignd.html-css-class-completion)
- [HTML CSS Support](https://marketplace.visualstudio.com/items?itemName=ecmel.vscode-html-css)

## Retocando los proyectos

En la siguiente página [Bootswatch.com](https://bootswatch.com/) encontraremos diferentes temas de bootstrap, para los estilos las clases a usar son exactamente igual. Según el tema que nos guste importamos de la misma manera usando el CDN [cdnjs](https://cdnjs.com/libraries/bootswatch).

<br/>

Para que funcionen algunos eventos de boostrap (por ejemplo el navbar responsive o el dropdown) importamos el CDN de javascript de boostrap.

```
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
```

Recordar que los scripts generalmente van justo antes de la etiqueta de cierre del body `</body>`

</br>

Es posible también, hacer uso del componente card de bootstrap para nuestros proyectos.

